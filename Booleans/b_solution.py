# Task 3

def calculate_bmi(weight, height, units):
    bmi = weight / (height * height)
    us_offset = 703

    # Consider the units. x *= 3 is the short-hand for x = x * 3, so multiply the given value by 3 and reassign it.
    if units == 'us':
        bmi *= us_offset

    output = 'Your BMI is {:.2f} - that\'s considered '.format(bmi)

    if bmi < 18.5:
        output += 'underweight.'
    elif bmi < 25:
        output += 'normal weight.'
    elif bmi < 30:
        output += 'overweight.'
    elif bmi < 35:
        output += 'obese.'
    else:
        output += 'extreme obese.'

    print(output)
    return bmi


# Task 4

OFFSET = 32
CELSIUS_GRADIENT = 1.8
FAHRENHEIT_GRADIENT = 1 / CELSIUS_GRADIENT


def convert_to_fahrenheit(celsius):
    # calculate Fahrenheit temperature and prepare a statement.
    temperature = celsius * CELSIUS_GRADIENT + OFFSET
    output = 'You\'re having {:.1f} °F. '.format(temperature)

    # call the suggestion function with the input and the statement and return the temperature.
    suggest_clothing(output, celsius)
    return temperature


def convert_to_celsius(fahrenheit):
    # Calculate the Celsius temperature and prepare a statement.
    temperature = (fahrenheit - OFFSET) * FAHRENHEIT_GRADIENT
    output = 'You\'re having {:.1f} °C. '.format(temperature)

    # Call the suggestion function with the result and the statement and return the temperature.
    suggest_clothing(output, temperature)
    return temperature


def suggest_clothing(statement, celsius):
    if celsius <= 15:
        statement += 'We suggest wearing warm clothes.'
    elif celsius <= 20:
        statement += 'We suggest wearing modest clothes.'
    elif celsius <= 25:
        statement += 'We suggest wearing light clothes.'
    else:
        statement += 'We suggest wearing no clothes at all.'
    print(statement)


# Check the solutions.
calculate_bmi(79, 1.78, 'metric')
calculate_bmi(120, 70, 'us')
convert_to_fahrenheit(32)
convert_to_celsius(32)
