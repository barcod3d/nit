def greet(time):
    # If the time is below 12:00 print morning statement.
    if time < 12:
        print('Good morning!')

    # Else if the time is below 4:00 print this statement
    elif time < 6:
        print('early riser, huh?')

    # Else if the time is greater equal than 18:00 print evening statement.
    elif time >= 18:
        print('Good evening!')

    #  Else print hello, i.e. if both of the given cases fail.
    else:
        print('Hello there!')


def gruesse(time):
    # If the time is below 12:00 print morning statement.
    if time < 12:
        print('Guten Morgen!')

    # Else if the time is greater equal than 18:00 print evening statement.
    elif time >= 18:
        print('Guten Abend!')

    #  Else print hello, i.e. if both of the given cases fail.
    else:
        print('Griass de!')

# greet(4.23)


def speed_limit_austria(road, vehicle):
    # If you drive in a car
    if vehicle == 'car':

        # And if you drive on the Autobahn your speed limit is 80 km/h.
        if road == 'Autobahn':
            print('Your speed limit is 130 km/h.')

        elif road == 'town road':
            print('Your speed limit is 50 km/h')

        else:
            print('Your speed limit is 100 km/h')

    # Else print a no data statement
    else:
        print('We have no speed limit data for your combination of vehicle and road.')


# speed_limit_austria('car', 'town road')