## Task 3
Merge your BMI functions from the simple functions section by adding an additional parameter units, which supports the
input us or metric. Update the function to print a statement of underweight, average weight or overweight along 
the return of the BMI.

## Task 4
Modify your converter functions by adding a clothing suggestion that is dependent on the given temperature - and no we 
don't expect people to be nude when it snows outside 😁