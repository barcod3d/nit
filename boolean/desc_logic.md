# Boolean Logic

This is a core concept for control flow in applications. Without diving too deep into mathematical theory, Boolean 
Algebra is a mathematical concept over the binary or logic set {0, 1} or {`False`, `True`} and the logical operators 
`and` (conjunction), `or` (disjunction) and `not` (negation): 

- **and**: Given two boolean values `A, B` the expression `A and B` is true if and only if `A` is true and `B` is true. 
- **or**: Given two boolean values `A, B` the expression `A or B` is false if and only if `A` is false and `B` is false. 
          That means if either `A` or `B` or _both_ are true, the expression is true. 
- **not**: Given a Boolean value `A`. If `A` is true then the expression `not A` is false and vice versa. 
 
and certain properties and rules that are applied to it, the [Peano Axioms](https://en.wikipedia.org/wiki/Peano_axioms)

These are typically expressed in truth tables, as shown next: 

| A     | B           | A and B  | A or B | not A |
|:-----:|:-----:| :----:| :----:| :----:|
| True  | True  | True  | True  | False |
| True  | False | False | True  | False |
| False | True  | False | True  | True  |
| False | False | False | False | True  |

## Conditions 

Conditions are comparative constructs that evaluate into a boolean value. These are primarily:

| Description | Expression | Example | Example value |
|:-----:|:-----:| :----:| :----:| 
| equals | `a == b`  | `3 == 5`  | False |
| not equals  | `a != b` | `3 != 5` | True  |
| less than | `a < b`  | `3 < 3` | False  |
| less equals | `a <= b`  | `3 <= 3` | True  |
| greater than | `a > b` | `5 > 3` | True |
| greater equals | `a >= b` | `2 >= 3` | False |

As each Condition is basically a boolean expression itself, they can be connected with the boolean operators as well. 
For example you want to check whether a number is in a given range of 1 and 6, both inclusive you can check that by the 
term `(a >= 1) and (a <= 6)`.  

## Control flow 

These conditions are typically used to regulate the control flow of your application. In order to do so we can use the 
`if` `elif` (else if) and `else` keywords, also known as **conditionals** as demonstrated below. Please note that only 
intended code will be included in the conditional case.  

```python
def greet(time):
    # If the time is below 12:00 print morning statement.
    if time < 12:
        print('Good morning!')
    
    # Else if the time is below 4:00 print this statement
    elif time < 6:
        print('early riser, huh?')

    # Else if the time is greater equal than 18:00 print evening statement.
    elif time >= 18:
        print('Good evening!')

    #  Else print hello, i.e. if both of the given cases fail.
    else:
        print('Hello there!')


greet(4.23)
```

If you execute this code you'll notice `Good morning!` will be printed. At this point you might wonder why because the
second clause is more specific than the first. However, the code gets executed sequentially, that means the first 
fitting case will be chosen and the other cases, i.e. all other other code until after the if control flow block, will 
be ignored. So you have to be aware of the order of cases given. 

Furthermore these conditionals can be nested as well. You could in theory combine these with an `and` operator, but you 
will eventually duplicate some code, which you should avoid as much as possible, but see Code Quality for that.  

```python
def speed_limit_austria(road, vehicle):
    # If you drive in a car
    if vehicle == 'car':

        # And if you drive on the Autobahn your speed limit is 80 km/h.
        if road == 'Autobahn':
            print('Your speed limit is 130 km/h.')

        elif road == 'town road':
            print('Your speed limit is 50 km/h')

        else:
            print('Your speed limit is 100 km/h')
    
    # Else print a no data statement
    else: 
        print('We have no speed limit data for your combination of vehicle and road.')
```