# BMI

Last time you updated the BMI module to take user input. You now want to make it a fully fleshed CLI application. 
You can use the solution_loops.py as a basic skeleton. On startup, welcome the users and inform them on 
preset preferences. Here's a list of supported commands:

- `quit` exits the application.
- `help` prints a helping text that indicates which commands are available.
- `change units` changes the units from metric to us or vice versa, dependent on the current state.
- `bmi <weight> <height>` calculates the bmi based on the given weight and height. 
- `convert height <value>` converts the given value (in the preset unit) to the respective other.
- `convert weight <value>` converts the given value (in the preset unit) to the respective other. 


## Hints 

- Applications are typically kept alive `while` the user does not enter the command to exit.
- This application has two internal states, metric units and us units. *Two states* are typically represented 
  using boolean values, so `True` and `False`.
- The conversions should be based on the preset unit. The return type float is there on purpose. Avoid duplicate 
  code by merging them into a single command function. 
