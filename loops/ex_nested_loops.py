# Some data to show
people = {
    1: {'name': 'Ali', 'age': 23, 'gender': 'cis male', 'city': 'London'},
    2: {'name': 'Bob', 'age': 26, 'gender': 'trans male', 'city': 'Vienna'},
    3: {'name': 'Chloe', 'age': 18, 'gender': 'cis female', 'city': 'Paris'},
    4: {'name': 'Dalaja', 'age': 41, 'gender': 'trans female', 'city': 'New Delhi'},
    5: {'name': 'Ehan', 'age': 50, 'gender': 'non-binary', 'city': 'Baghdad'},
}


def print_info(people):
    """
    Prints the information of a given people dictionary.

    :param people: A dictionary which holds (id, info) tuples, where the info is another dict itself.
    :type people: dict
    """
    for p_key, p_value in people.items():
        print('\nPerson ID: {}'.format(p_key))

        for key in p_value:
            print('{}: {}'.format(key, p_value.get(key)))


print_info(people)
