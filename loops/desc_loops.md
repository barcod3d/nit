# Loops

Loops are iterative repetitions to execute a block of statements until a certain condition is reached. Some typical
examples would be to keep your application running, iterative calculations or to iterate over sequential data. Loops
cannot have an empty body, but if you want to have one for whatever reason you can use the `pass` statement.

## for loop

Python's for loop is used to iterate over sequential data, e.g., list, dictionary, ... Its syntax
is `for <it> in <data>`, with `<it>`
as a iterator name referring to the individual object of the list.

```python
def greet(people):
    greeting = ''
    for person in people:
        greeting += 'Hello ' + person + '! '
    return greeting
```

The loop also has 3 important statements:

- `break`: will stop your loop, independently on whether you are done looping or not.
- `continue`: will stop your current iteration and will continue with the next.
- `else`: The code inside the else block will only be executed on full looping.

```python
def greet_friends(people, friends):
    greeting = ''
    for person in people:
        if person in greeting:  # If we greeted the person, cancel. 
            break
        elif person in friends:  # If we haven't greeted your friend yet, do so.
            greeting += 'Hello ' + person + '! '
        else:  # If the person is not your friend, skip. 
            continue
    else:  # This will only be reached if we don't break beforehand. 
        greeting += 'How are you?'
    return greeting
```

## range

Sometimes programmers want to iterate over a range of numbers, given a starting and ending index and individual steps to
de- and increment. For that purpose Python has a built-in function `range(start, stop, step)` and its shorthand versions
`range(start, stop)` and `range(stop)`.

```python
# For i = 0 up to 10 exclusive print i and increment i by one. 
for i in range(0, 10, 1):
    print(i)

# Shortened with default increment: 
for i in range(0, 10):
    print(i)

# Further shortened with default parameters: 
for i in range(10):
    print(i)

# For j = 10 down to 0 exclusive print j and decrement j by two. 
for j in range(10, 0, -2):
    print(j)
```

The range function's `start` parameter is inclusive, whereas the `stop` parameter is exclusive and all values are
integers and not floating point numbers. This means the loop's condition checks whether `iterator < stop` for increment
and `iterator > stop` for decrement. If you want inclusive comparison, so `iterator <= stop` you have to use
`iterator < stop + 1`, or `iterator >= stop` you have to use `iterator > stop - 1`. This is valid because all numbers
are integers. We can only use additive steps, so no multiplication or division or the like.

The `range()` function creates a so called `iterable` object, which you can cast, i.e., convert to other sequential data
types like lists:

```python
x = list(range(0, 10, 2))  # [0, 2, 4, 6, 8]
```

Which you can in return use in your code *without* needing a for loop, e.g. you can use it in the *while* loop, too!

### dynamic sequences

So far we had static numbers as stop points for our ranges. However, typically these are not static, say if the content
of the list is dependent on user input. Python has another built-in function `len(obj)` that will return the number of
items in the containing object:

```python
def greet(people):
    greeting = ''
    for i in range(len(people)):
        greeting += 'Hello ' + people[i] + '! '
    return greeting
```

Now the exclusive stop in the `range()` function makes sense, too: Imagine the following list of names with their 
respective index:

```python
people = [(0, 'John'), (1, 'Heidi'), (2, 'Phil'), (3, 'John'), (4, 'Anna')]
```

The list contains 5 objects, so `len(people) == 5` but as you can see the maximum index is 4. Therefore, you don't need 
any additional index shifting. 

## while loops

This  `while <cond>` loop will execute its block while the boolean condition `<cond>` is evaluated to `True`. All *for*
loops can be translated to *while* loops but not vice versa:

```python
def greet(people):
    greeting = ''
    i = 0
    while i < len(people):
        greeting += 'Hello ' + people[i] + '! '
        i += 1
    return greeting
```

There are a few things to watch out for:

1. **Increase the iterator** to avoid infinite loops. If you do not increase it the condition will remain `True`
   infinitely.
2. **Watch your order** of statements when interacting with the iterator so that you won't work on false values. 

## Nested and complex loops

Some data structures are rather complex and even nested. You can however access multiple iterators at once, as well as
nest loops themselves:

```python
people = {
    1: {'name': 'Ali', 'age': 23, 'gender': 'cis male', 'city': 'London'},
    2: {'name': 'Bob', 'age': 26, 'gender': 'trans male', 'city': 'Vienna'},
    3: {'name': 'Chloe', 'age': 18, 'gender': 'cis female', 'city': 'Paris'},
    4: {'name': 'Dalaja', 'age': 41, 'gender': 'trans female', 'city': 'New Delhi'},
    5: {'name': 'Ehan', 'age': 50, 'gender': 'non-binary', 'city': 'Baghdad'},
}


def print_info(people):
    for p_key, p_value in people.items():
        print('\nPerson ID: {}'.format(p_key))

        for key in p_value:
            print('{}: {}'.format(key, p_value.get(key)))
```

The variable `people` is a dictionary with `(key, value)` tuples as items, with `value` being another dictionary. You
can access these simultaneously as shown in the first loop or you can access just the key or value respectively, as 
discussed in the last lesson. 

## Avoiding unnecessary loops.

The function `add_natural_numbers(n)` does look like a good idea to solve the task. However, there is also another way
to solve it:

```python
def add_natural_numbers(max_val):
    """
    Adds natural numbers between 1 and max_val iteratively using a while loop.

    :param max_val: The upper boundary to include
    :type max_val: int
    :return: The sum from i = 1 to n
    :rtype: int
    """
    natural_sum = i = 0

    while i <= max_val:
        natural_sum += i
        i += 1  # Increase the index to avoid an infinite loop!

    return natural_sum


def gaussian_sum_formula(n):
    """
    Add natural numbers between 1 and n using the gaussian sum formula.

    :param n: The upper boundary to include
    :type n: int
    :return: The sum from i = 1 to n
    :rtype: int
    """
    sum = int((n * n + n) / 2)
    return sum
```

Programming is often a trade-off between efficiency and readability. While the iteration does make more sense, so it is
more easily readable, and a lot easier to figure out, the second one is *a lot* more effective. Loops scale linearly
with the input while fixed calculations take a more or less constant time:

```
With n = 5, executing gaussian_sum_formula(n) for 10,000 times takes 1152 nanoseconds.
With n = 5, executing add_natural_numbers(n) for 10,000 times takes 2800 nanoseconds.
With n = 50, executing gaussian_sum_formula(n) for 10,000 times takes 1407 nanoseconds.
With n = 50, executing add_natural_numbers(n) for 10,000 times takes 22447 nanoseconds.
```

These times are dependent on the executed machine. Nested loops are even worse because they scale exponentially, so you
should try to avoid these, if possible. 