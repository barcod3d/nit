all_people = ['John', 'Heidi', 'Phil', 'John', 'Anna']
friends = ['John', 'Phil']


def greet(people):
    """
    Greets everyone in a list of people.

    :param people: The list of people to greet individually.
    :type people: list
    :return: The greeting as a string.
    :rtype: str
    """
    greeting = ''
    for person in people:
        greeting += 'Hello ' + person + '! '
    return greeting


def greet_friends(people, friends):
    """
    Greets certain friends in a list of people.

    :param people: The list of people.
    :type people: list
    :param friends: The list of friends to greet individually. .
    :type friends: list
    :return: The greeting as a string.
    :rtype: str
    """
    greeting = ''
    for person in people:
        if person in greeting:  # If we greeted the person, cancel.
            break
        elif person in friends:  # If we haven't greeted your friend yet, do so.
            greeting += 'Hello ' + person + '! '
        else:  # If the person is not your friend, skip.
            continue
    else:  # This will only be reached if we don't break beforehand.
        greeting += 'How are you?'
    return greeting



def greet_for(people):
    """
    Same as greet(people) but with a while loop.
    """
    greeting = ''
    for i in range(len(people)):
        greeting += 'Hello ' + people[i] + '! '
    return greeting

def greet_while(people):
    """
    Same as greet(people) but with a while loop.
    """
    greeting = ''
    i = 0
    while i < len(people):
        greeting += 'Hello ' + people[i] + '! '
        i += 1
    return greeting


# results
print(greet(all_people))
print(greet_while(all_people))
print(greet_for(all_people))
print(greet_friends(all_people, friends))

