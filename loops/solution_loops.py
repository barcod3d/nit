# Some maybe useful variables
units = True
is_running = True

def convert_to_lbs(kg):
    """
    Converts a given kg weight to a lbs weight, i.e., converts from metric weight unit to us one.

    :param kg: The weight in kilograms.
    :type kg: float
    :return: The weight in lbs.
    :rtype: float
    """


def convert_to_kg(lbs):
    """
    Converts a given lbs weight to a kg weight, i.e. converts from us weight unit to metric one.

    :param lbs: The weight in lbs.
    :type lbs: float
    :return: The weight in kg.
    :rtype: float
    """


def convert_to_inch(m):
    """
    Converts a given m height to an inch height, i.e., converts from metric height unit to us one.

    :param m: The height in meters.
    :type m: float
    :return: The height in inch.
    :rtype: float
    """


def convert_to_meter(inch):
    """
    Converts a given inch height to an meter height, i.e., converts from us height unit to metric one.

    :param m: The height in inch.
    :type m: float
    :return: The height in meters.
    :rtype: float
    """


def convert_to_feet(inch):
    """
    Converts a given inch height to inch and feet, as the height is commonly represented in feet and inch, e.g. 6'10".

    :param inch: The height in inch.
    :type inch: float
    :return: The height in feet and inch.
    :rtype: str
    """


def toggle_units():
    """
    Changes the units from metric to us or vice versa.

    :return: A message that indicates it's success and current state.
    :rtype: str
    """