# Ignore the first statement, this is used for illustration only, you will understand later on.
import timeit


def add_natural_numbers(max_val):
    """
    Adds natural numbers between 1 and max_val iteratively using a while loop.

    :param max_val: The upper boundary to include
    :type max_val: int
    :return: The sum from i = 1 to n
    :rtype: int
    """
    natural_sum = i = 0

    while i <= max_val:
        natural_sum += i
        i += 1  # Increase the index to avoid an infinite loop!

    return natural_sum


def gaussian_sum_formula(max_val):
    """
    Adds natural numbers between 1 and max_val using the gaussian sum formula.

    :param max_val: The upper boundary to include
    :type max_val: int
    :return: The sum from i = 1 to n
    :rtype: int
    """
    gaussian_sum = int((max_val * max_val + max_val) / 2)
    return gaussian_sum


# results:
n = 50

print('Iterative result for n = {} is {}.'.format(n, add_natural_numbers(n)))
print('Gaussian result for n = {} is {}.'.format(n, gaussian_sum_formula(n)))

# Measure times for illustration - You don't need to understand the code as of yet, don't worry.
time_gs_ns = timeit.timeit('gaussian_sum_formula({})'.format(n), 'from __main__ import gaussian_sum_formula',
                           number=10000) * 1000 * 1000
time_it_ns = timeit.timeit('add_natural_numbers({})'.format(n), 'from __main__ import add_natural_numbers',
                           number=10000) * 1000 * 1000

print('With n = {}, executing gaussian_sum_formula(n) for 10,000 times takes {:.0f} nanoseconds.'.format(n, time_gs_ns))
print('With n = {}, executing add_natural_numbers(n) for 10,000 times takes {:.0f} nanoseconds.'.format(n, time_it_ns))
