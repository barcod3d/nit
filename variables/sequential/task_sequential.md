## Task 5 : Sequences

You are a developer for a famous nudist app *iNsdr* and you are supposed to write the business logic for showing profile
details and potential matches. The data you get from your MongoDB database is similar to:

```json
{
  "username": "HotSteve85",
  "email": "StevenLHelister@eirot.com",
  "personal information": {
    "name": "Steven L. Hester",
    "date of birth": "1985-03-15",
    "gender": "male",
    "orientation": "bi-curious",
    "size": 5.6,
    "weight": 142.1
  },
  "location": {
    "street": "3051 Jody Road",
    "city": "Shillington",
    "zip": 19067,
    "state": "PA"
  },
  "hobbies": [
    "Hiking",
    "Swimming",
    "Gym",
    "Music"
  ]
}
```

However, due to privacy reasons not all of that data should be displayed on the app. So your first subtask is to create
a function `filter(person)` which takes such a person, filters out sensitive data and returns a **new** dictionary with
filtered data to be shown as profiles:

- username
- first name
- age / year of birth
- gender
- orientation
- size
- weight
- city
- state
- hobbies

The next thing you want to create is a function `match(this_person, other_person)` that matches people with the same
hobbies and location, i.e., city or state that's up to you.

Hints: You can use conditionals to check for location equality. However, for the hobbies as we haven't talked about loops
yet, you might want to use Python's set. Python has a built-in function `set(iterable)`, which takes an iterable
sequence like lists, tuples or dictionaries and converts it to a set.  

