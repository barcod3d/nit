input_a = {
    "username": "HotSteve85",  # show
    "email": "StevenLHelister@eirot.com",  # hide
    "personal information": {
        "name": "Steven L. Hester",  # first name only
        "date of birth": "1985-03-15",  # show year of birth
        "gender": "male",  # show
        "orientation": "bi-curious",  # show
        "size": 5.6,  # show
        "weight": 142.1  # show
    },
    "location": {
        "street": "3051 Jody Road",  # hide
        "city": "Shillington",  # show
        "zip": 19067,  # hide
        "state": "PA"  # show
    },
    "hobbies": ["Hiking", "Swimming", "Gym", "Music"]  # show
}
input_b = {
    "username": "LatinLover",  # show
    "email": "MiguelSchneider@eirot.com",  # hide
    "personal information": {
        "name": "Miguel Schneider",  # first name only
        "date of birth": "1942-01-19",  # show year of birth
        "gender": "male",  # show
        "orientation": "straight",  # show
        "size": 5.5,  # show
        "weight": 174.0  # show
    },
    "location": {
        "street": "2579 Custer Street",  # hide
        "city": "Altoona",  # show
        "zip": 16601,  # hide
        "state": "PA"  # show
    },
    "hobbies": ["Cooking", "Fishing"]  # show
}


def privacy_filter(person_full):
    """
    Filters sensitive user data retrieved from the MongoDB database, given as a JSON object.

    :param person_full: All available information for a given person as a JSON object.
    :type person_full: dict
    :return: Filtered information for the given data.
    :rtype: dict
    """
    person = person_full.copy()

    # the keys to filter completely:
    person.pop("email")
    person['location'].pop("street")
    person['location'].pop("zip")

    # Just use the first name:
    name = person['personal information']['name'].split(" ")
    person['personal information']['name'] = name[0]

    # Show the year of birth
    dob = person['personal information']['date of birth'].split("-")
    person['personal information']['date of birth'] = dob[0]

    return person


def match(person_a, person_b):
    """
    Matches two persons based on their location and hobbies.

    :param person_a: The first person to match.
    :type person_a: dict
    :param person_b: The second person to match
    :type person_b: dict
    :return: True, if the two match, False else
    :rtype: bool
    """
    # Match the location based on the state.
    is_match = person_a['location']['state'] == person_b['location']['state']

    # We don't need to continue if the locations do not match.
    if is_match:
        # Convert the hobbies to sets and check for intersection and format accordingly
        matches = set(person_a['hobbies']).intersection(set(person_b['hobbies']))
        is_match = bool(matches)  # Will be True if the intersection is not empty.

    return is_match


def match_details(person_a, person_b):
    """
    Matches two persons based on their location and hobbies and print details to the console.

    :param person_a: The first person to match.
    :type person_a: dict
    :param person_b: The second person to match
    :type person_b: dict
    :return: True, if the two match, False else
    :rtype: bool
    """
    # The information string we'll print.
    info = '{} and {} do '.format(person_a['username'], person_b['username'])

    # Match the location based on the state.
    is_match = person_a['location']['state'] == person_b['location']['state']

    # We don't need to continue if the locations do not match.
    if not is_match:
        info += 'not match, based on their location.'

    else:
        info += 'match based on their location {}'.format(person_a['location']['state'])

        # Convert the hobbies to sets and check for intersection and format accordingly
        matches = set(person_a['hobbies']).intersection(set(person_b['hobbies']))

        # Will be True if the intersection is not empty.
        is_match = bool(matches)

        # We want proper grammar :D
        hobby_grammar = 'hobby' if len(matches) == 1 else 'hobbies'

        # Add the matching hobbies properly formatted.
        if is_match:
            matches = str(matches).replace('{', '').replace('}', '').replace('\'', '').lower()
            info += ' and their ' + hobby_grammar + ' ' + matches + '.'
        else:
            info += " but do not match based on their " + hobby_grammar + '.'

    # Print the information and return the Boolean.
    print(info)
    return is_match


person = privacy_filter(input_a)
print(str(person) + '\n')

# match is working for both filtered and unfiltered persons.
print(match(person, input_b))
match_details(person, input_b)