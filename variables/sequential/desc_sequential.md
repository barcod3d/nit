# Sequential Data

Let's have a slightly closer look at sequential Data. Python has some native sequential data. Before we talk about what 
these are let's look at their properties.

## Properties 

- **Order**: Some of these sequential data types are ordered, that means each item in their sequence has a concrete 
  position, or also known as *index*, which is typically used to access the item.
- **Changeability**: In some cases you can easily add and remove more data to the sequence while others are somewhat 
  fixed.
- **Duplicates**: Some sequences allow duplicated entries, while others don't. 

### Index 

The index is an integer number, i.e., a whole number in the range of your sequential data's length - 1, because most 
sequential data structures are 0-indexed. So, the first element has the index 0. the index is depicted in square 
brackets behind the sequence. You can use an index range, like `[i:j]`, also. In this case `i` is inclusive while `j` 
is not and both are optional. 

```python
my_string = "Hello"

# the length of this string is 5, because it consists of 5 characters. 
print('my_string has length of {}.'.format(len(my_string)))

# However, the indexes are as following:
print(my_string[0])  # H
print(my_string[1])  # e
print(my_string[2])  # l
print(my_string[3])  # l
print(my_string[4])  # o

print()

# We can use negative indexes to reverse the order:
print(my_string[-1])  # o
print(my_string[-2])  # l
print(my_string[-3])  # l
print(my_string[-4])  # e
print(my_string[0])  # H

# Get a sub sequence using the index:
print(my_string[1:3])  # el
print(my_string[3:])  # lo
print(my_string[:4])  # Hell

```

## Data types 

| Data type | Order | Changeable | Duplicates |
| --------- | :------: | :--------: | :--------: |
| String | x | x | x |
| List | x | x | x |
| Tuple | x | | x |
| Dictionary | x | x | |
| Set | | x | | 


### String

You're already quite familiar with the string data type. A string is an array of characters, that means each 'item' is 
considered a character, i.e., a letter or a sign. You can basically use it like specially formatted list, except for not
having multiple data types as items. That means you can access individual or a sequence of characters using the index.

Strings have some nice functionality along, like the `.format()` you are already familiar with: 

```python
# You can replace substrings:
print(my_string.replace('o', 'u') + '\n')  # Hellu

# You can modify the cases:
print('my_string in lower case: ' + my_string.lower())  # hello
print('my_string in upper case: ' + my_string.upper())  # HELLO


# Checks the presence or absence of certain subsequences, a.k.a. substrings:
if 'World' in my_string:
    print(my_string)
elif 'World' not in my_string:
    print('No World to greet.')
```

There are a lot of other functions, but we won't be going into further details for now - if you are interested please 
feel free to look at the [documentation](https://docs.python.org/3/library/stdtypes.html). Just one last important thing 
to mention however are so called *escape characters*. These are characters that would be illegal to insert regularly 
because they are used to format your code. You typically use the backslash to express these, for example:

- `\'` or `\"` to print a single or double quote. 
- `\\` to print a backslash.
- `\n` to print a new line. 
- ...

### Lists

Lists are another sequential datatype used to store multiple items in a single variable. Just like strings, lists are 
ordered with an index and do allow duplicates. Unlike other programming languages and their array datatype, Python lists 
do allow mixed data types of their items:

```python
# A person as a list with a given first name and an age. 
person = ["John", 42]
```

You can access items with the index just like shown before, however you can also append and insert lists with predefined
functions: 

```python
# A person as a list with a given first name and an age.
person = ["John", 42]

# A list of interests:
interests = ["Hiking", "Swimming", "Socializing"]

# We now want to add the person's gender and last name.
person.append('male')
person.append('Doe')
print(person)

# We now realize we want to have the last name right after the first name
person.pop()
person.insert(1, 'Doe')
print(person)

# We can also extend the person with the interests:
person.extend(interests)

print('Person with interests {}.'.format(person))

# But we can also remove them, but just slightly more complex (see loops for details):
for interest in interests:
    person.remove(interest)

print('Person without interests {}.'.format(person))
```

Note on `pop()`: The list is considered a *stack*, i.e. data is placed on top of another and is accessed in a *last in -
first out* fashion. Placing an item on top of the stack is typically called *push* and removing this is called *pop*. 
You can also have an optional index if you want to remove an item at a specific position. 

If you want to remove a specific value, regardless of its position, you should use the `remove()` function. 


### Tuples 

Tuples are similar to a list, except for being **unchangable**. However there is a way around it:

```python
# A person tuple.
person = ("John", 42)

# If you want to add, update or delete the values, you need to use a list:
temp = list(person)
temp[1] += 1
person = tuple(temp)
print(person)  # ('John', 43)
```

You can furthermore simply join tuples: 
```python
# If you want to join tuples you can do so easily
interests = ("Hiking", "Swimming", "Socializing")
print(person + interests)  # ('John', 43, 'Hiking', 'Swimming', 'Socializing')
```

### Dictionaries

Dictionaries store their data in a key:value pair. As the name key suggests, these have to be unique, duplicates will 
just overwrite the previous ones. Dictionaries can contain other dictionaries, so called *nesting*.  
```python
# Person as a dictionary
person = {
    "name": "John",
    "age": 42,
}

interests = {
    "Hiking": "Alpine",
    "Swimming": "Sea"
}

# Accessing the values stored is possible using the key instead of an index
info = person["age"]
print(info)  # 42

# But you can also call the get() function on the key:
info = person.get("name")
print(info) # John

# In order to get all potential keys or values you can:
print(person.items())  # dict_items([('name', 'John'), ('age', 42)])
print(person.keys())  # dict_keys(['name', 'age'])
print(person.values())  # dict_values(['John', 42])

# Update to values are handled similarly
person["age"] = 43
person.update({"name": "John Doe"})

# If the key doesn't exist it will instead add a new one:
person["gender"] = "male"
person.update({"interests": interests})
person.update({"interests_c": interests.copy()})
print(person) 
# {... 'interests': {'Hiking': 'Alpine', 'Swimming': 'Sea'}, 
# 'interests_c': {'Hiking': 'Alpine', 'Swimming': 'Sea'}}

# Note assigning dicts will not copy the value but it will just link the pointer:
interests.update({"Swimming": "Pool"})
print(person)
# {... 'interests': {'Hiking': 'Alpine', 'Swimming': 'Pool'}, 
# 'interests_c': {'Hiking': 'Alpine', 'Swimming': 'Sea'}}

# If you want to remove an information you can pop specific or last:
person.pop("gender")
person.popitem()
print(person) # {'name': 'John Doe', 'age': 43, 'interests': {...}}
```

### Sets

Python sets are data collections in the mathematical sense of sets, i.e. the data is unordered and duplicate-free. As 
such you cannot access data by an index. This might sound underwhelming at first, however, you can use all mathematical
set operations with them - which makes them quite unique and this is one of the reasons why Python is so 
popular among data engineers and anyone working on databases! :)

```python
# Assigning sets
favorite_food = {"Pizza", "Burger", "Stew", "Apples", "Tomatoes"}
fruits = {"Apples", "Bananas", "Kiwi", "Pears", "Strawberries"}
vegetables = {"Tomatoes", "Salad", "Eggplant"}

# Looking at your favorite food you'll notice something's off:
favorite_food.remove("Stew")
favorite_food.add("Kebab")

# We cannot access items directly but we can check containment:
available_food = "Kebab"
if available_food in favorite_food:
    print(available_food + " is part of your favorite food.")

# And we can use the set operators:
healthy_food = fruits.union(vegetables)
eat_healthy = favorite_food.intersection(healthy_food)
eat_unhealthy = favorite_food.difference(healthy_food)
print('you eat healthy if you eat your favorite food {}'.format(eat_healthy))
print('you eat unhealthy if you eat your favorite food {}'.format(eat_unhealthy))
```
