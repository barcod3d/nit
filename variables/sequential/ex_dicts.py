# Person as a dictionary
person = {
    "name": "John",
    "age": 42,
}

interests = {
    "Hiking": "Alpine",
    "Swimming": "Sea"
}

# Accessing the values stored is possible using the key instead of an index
info = person["age"]
print(info)

# But you can also call the get() function on the key:
info = person.get("name")
print(info)

# In order to get all potential keys or values you can:
print(person.items())
print(person.keys())
print(person.values())

# Update to values are handled similarly
person["age"] = 43
person.update({"name": "John Doe"})

# If the key doesn't exist it will instead add a new one:
person["gender"] = "male"
person.update({"interests": interests})
person.update({"interests_c": interests.copy()})
print(person)

# Note assigning dicts will not copy the value but it will just link the pointer:
interests.update({"Swimming": "Pool"})
print(person)

# If you want to remove an information you can pop specific or last:
person.pop("gender")
person.popitem()
print(person)