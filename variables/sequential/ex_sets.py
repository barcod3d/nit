# Assigning sets
favorite_food = {"Pizza", "Burger", "Stew", "Apples", "Tomatoes"}
fruits = {"Apples", "Bananas", "Kiwi", "Pears", "Strawberries"}
vegetables = {"Tomatoes", "Salad", "Eggplant"}

# Looking at your favorite food you'll notice something's off:
favorite_food.remove("Stew")
favorite_food.add("Kebab")

# We cannot access items directly but we can check containment:
available_food = "Kebab"
if available_food in favorite_food:
    print(available_food + " is part of your favorite food.")

# And we can use the set operators:
healthy_food = fruits.union(vegetables)
eat_healthy = favorite_food.intersection(healthy_food)
eat_unhealthy = favorite_food.difference(healthy_food)
print('you eat healthy if you eat your favorite food {}'.format(eat_healthy))
print('you eat unhealthy if you eat your favorite food {}'.format(eat_unhealthy))
