my_string = "Hello"

# the length of this string is 5, because it consists of 5 characters. 
print('my_string has length of {}. \n'.format(len(my_string)))

# However, the indexes are as following:
print(my_string[0])  # H
print(my_string[1])  # e
print(my_string[2])  # l
print(my_string[3])  # l
print(my_string[4] + '\n')  # o

# We can use negative indexes to reverse the order:
print(my_string[-1])  # o
print(my_string[-2])  # l
print(my_string[-3])  # l
print(my_string[-4])  # e
print(my_string[0] + '\n')  # H

# Get a sub sequence using the index:
print(my_string[1:3])  # el
print(my_string[3:])  # lo
print(my_string[:4] + '\n')  # Hell

# You can replace substrings:
print(my_string.replace('o', 'u') + '\n')  # Hellu

# You can modify the cases:
print('my_string in lower case: ' + my_string.lower())  # hello
print('my_string in upper case: ' + my_string.upper())  # HELLO


# Checks the presence or absence of certain subsequences, a.k.a. substrings:
if 'World' in my_string:
    print(my_string)
elif 'World' not in my_string:
    print('No World to greet.')


