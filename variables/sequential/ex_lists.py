# A person as a list with a given first name and an age.
person = ["John", 42]

# A list of interests:
interests = ["Hiking", "Swimming", "Socializing"]

# We now want to add the person's gender and last name.
person.append('male')
person.append('Doe')
print(person)

# We now realize we want to have the last name right after the first name
person.pop()
person.insert(1, 'Doe')
print(person)

# We can also extend the person with the interests:
person.extend(interests)

print('Person with interests {}.'.format(person))

# But we can also remove them, but just slightly more complex (see loops for details):
for interest in interests:
    person.remove(interest)

print('Person without interests {}.'.format(person))






