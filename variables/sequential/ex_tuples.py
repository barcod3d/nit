# A person tuple.
person = ("John", 42)

# If you want to add, update or delete the values, you need to use a list:
temp = list(person)
temp[1] += 1
person = tuple(temp)
print(person)

# If you want to join tuples you can do so easily
interests = ("Hiking", "Swimming", "Socializing")
print(person + interests)