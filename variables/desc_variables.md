# Variables and constants

Variables are used to store data at different levels of scopes. In contrast to some other programming languages python 
has no command to declare variables but instead the variable is created the moment you first assign a value to it and 
the datatype is dependent on your assignment. 

## Naming conventions 

In contrast to some other programming languages **variables** are per convention named in `snake_case`, 
that means lower case words that are connected by an underscore, like `my_fancy_variable`. The names should somewhat 
resemble what data and for what purpose you want to store the data.

**Constants** are fixed values that are not supposed to change during the execution of your program. The constants are 
named in `MACRO_CASE`, that means upper case words that are connected by an underscore, like `MY_FANCY_CONSTANT`. 
They are typically used  to avoid repetitive values or to avoid so called *magic numbers*, i.e. 
numeric values directly placed in your statements with an implicit meaning that might be hard to understand later on.

## Assignment 

You can assign data to variables and constants in several ways:

- 1:1 so one value to one variable, e.g. `name = 'John'`
- N:M so multiple values to multiple variables, e.g. `first_name, last_name = 'John', 'Doe'` 
- 1:N one value to multiple variables, e.g. `johns_age = annas_age = 18` where `18` is the value assigned to the 
  variables `johns_age` and `annas_age`. 
- unpacking, where you have a sequence of data and want to assign each value to a variable, so e.g. you have a list 
  `ingredients = ['Water', 'Flour', 'Eggs']` you can assign these to `liquid, carb, proteine = ingredients`, so `liquid` 
  will have the value `'Water'` and so on. 
  

## Built-in data types.   

Data-types are also an important concept and they decide how the data is represented internally as well as what kind of operations you can do with the data. Python has several built in ones but of course you can always extend these by defining your own (we will do that later on). 
- Text type, which is string (str, e.g. "Hello" or 'Hello')
- numeric types, like integers (int, e.g. 1) or floating point numbers (float, e.g. 1.5)
- sequence types which are lists or tuples
- mapping type, i.e. dictionaries (dict)
- booleans (bool) which are either True or False 
- binaries like bytes, byte arrays or memory view. 

Note that you *cannot* combine different datatypes, i.e. you cannot add a text and a numeric type but instead these need
 to be casted:
 
 ```python
first_str = "1"
first_num = 1 

second_str = "2" 
second_num = 2 

sum_str = first_str + second_str 
sum_num = first_num + second_num 

# This next line will fail, hence comment it, i.e. we tell the compiler to ignore it
# sum_str_num = first_str + second_num 

# However if we want to add them we can use casting: 
casted_num = int(first_str) + second_num 

print("If you try to add strings, you will get the result {}.".format(sum_str)) 
print("if you try to add numbers you will get the result {0} which equals the casted result {1}".format(sum_num, casted_num))
```

### String formatting

If you take a look at the last two statements you might notice expressions like `{}`. These are called placeholders or 
*string interpolation*. Their concept is especially valuable for dynamic texts, e.g. used to display the user's name 
for a logged in user at a website. 

```python
name = "variables"
condition = "fun to play with"
my_output = "{} are {}."
print(my_output.format(name, condition))
```

In the previous examples there were also given numbers within these brackets, the so-called indexes. These are used to 
arrange the data explicitly, so you could potentially swap the order:

```python
my_yoda_output = "{1} are {0}."
print(my_yoda_output.format(name, condition))
```

However, you *cannot* mix these two so a string `'{} are {0}.'` will cause an error. Furthermore, you can format the 
data. You can fill pages with formatting strings alone, so we aren't going into too much details here. Just one kind of 
important concept is formatting floating point numbers. Naturally humans want to deal with only a few digits and prefer
to align these. We can use `{i:w.df}` to do so, where `i` is the index, `w` is the total width, `d` is the amount of
digits after the dot and `f` denotes fixed point notation. For example: 

```python
print('{:6.2f}'.format(3.145345))
print('{:6.2f}'.format(23.5962323))
print('{:6.2f}'.format(999.1452325))
```
will align all numbers evenly with 2 digits after the dot. The 6 comes from the 2 digits + the dot + the 3 digits before.
Of course you can leave the width alone, as well if you don't want alignments.

```python
PI = 3.14159265359
print('Pi is about {:.2f}.'.format(PI))
```
