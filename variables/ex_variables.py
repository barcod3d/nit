first_str = "1"
first_num = 1

second_str = "2"
second_num = 2

sum_str = first_str + second_str
sum_num = first_num + second_num

# This next line will fail, hence comment it, i.e. we tell the computer to ignore it. If you want to try just remove #.
# sum_str_num = first_str + second_num

# However if we want to add them we can use casting:
casted_num = int(first_str) + second_num

# Let's show some output
print("If you try to add strings, you will get the result {}.".format(sum_str))
print("if you try to add numbers you will get the result {0} which equals the casted result {1}".format(sum_num,
                                                                                                        casted_num))

# Placeholders
name = "variables"
condition = "fun to play with"
my_output = "{} are {}."
my_yoda_output = "{1} are {0}."

# Lets show some more output.
print()
print(my_output.format(name, condition))
print(my_yoda_output.format(name, condition))

# Format these please!
print()
print('{:6.2f}'.format(3.145345))
print('{:6.2f}'.format(23.5962323))
print('{:6.2f}'.format(999.1452325))

PI = 3.14159265359
print('Pi is about {:.2f}.'.format(PI))