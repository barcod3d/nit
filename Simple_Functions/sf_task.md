## Task 1 

Implement two functions `convert_to_fahrenheit(celsius)` and `convert_to_celsius(fahrenheit)` that take °C and °F numeric 
values as an input and return the value as the respective other.

## Task 2 
Implement two functions `calculate_bmi_metric(weight, height)` which takes metric units (kg and m) and 
`calculate_bmi_us(weight, height)` which takes US units (lb and in) that calculates the body mass index of a given
 weight and height.