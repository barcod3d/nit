# Task 1
# The conversion from °C to °F is a linear function of the shape f(x) = gradient * x + offset.

OFFSET = 32
CELSIUS_GRADIENT = 1.8
FAHRENHEIT_GRADIENT = 1 / CELSIUS_GRADIENT


def convert_to_fahrenheit(celsius):
    return celsius * CELSIUS_GRADIENT + OFFSET


def convert_to_celsius(fahrenheit):
    return (fahrenheit - OFFSET) * FAHRENHEIT_GRADIENT


# Task 2

def calculate_bmi_metric(kg, m):
    return kg / (m * m)


def calculate_bmi_us(lbs, inch):
    offset = 703
    return offset * lbs / (inch * inch)


# Check the solutions.
print(convert_to_fahrenheit(32))
print(convert_to_celsius(32))
print(calculate_bmi_metric(79, 1.78))
print(calculate_bmi_us(174, 70))
