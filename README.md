# NI Technology 

## Description 

This is a repository to keep the Python lessions organized in the NI Technology community. There will be lessions on the Discord and on here on a weekly timeframe but the Discord lessions will be deleted in order to keep things organized. However *all* lessions can be found in the repository in the respective folders for you to pull and practice on your local computer or the discord server itself.  

Of course you can still continue to practise on Discord directly but eventually our programs will become too complex, especially since the Discord bot can't handle `input` or `import` statements.

## Prerequisites 

Before you can start to program in Python on your local computer, you do need to have some things installed beforehand. 

### Python 3

First and foremost you need to have a *compiler* or *interpreter*, which is a program that translates your Python code into executable code. Since we are using Python as the programming language, we will also use the official Python, the latest Python 3 to be precise. You can get that [here](https://www.python.org/downloads/). 

### Integrated Development Environment 

Something else you might *want* to use is an IDE which takes over some useful tasks like exection of your code per button click and other useful things. The probably most commonly used IDE for Python is [PyCharm](https://www.jetbrains.com/pycharm/). There's a free community version that has Github included! 


## Order of events:

1. Introduction 
2. Simple Functions 
3. Booleans
