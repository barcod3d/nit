## Programming in Discord 

Before we start off with actual programming, let me illustrate how to use the Discord bot that will compile and execute 
our code. Discord’s chat is partially based on markdown language, which is also used to format these files, i.e. you can
 format your text using the most common MD commands.  Code is typically displayed by as a block of lines surrounded by 3
 `:
 
```
print('Hello World') 
```

Another concept of programming is called syntax highlighting, which gives you visual feedback of your code.  It 
increases your code's readability and thus your productivity.  If you add the program language just after the first 3 `,
 it will highlight the text according to your programming language. The code block above is supposed to resemble Python 3,
which we will be using over the next couple of weeks so lets just add a python to the block:

```python
print('Hello World') 
```

if we now want to execute the code using the bot, we need to add the `;python` command before code. You don't have to 
use either concepts but eventually you will have an easier time to read and understand code if it is syntactically 
highlighted. 

