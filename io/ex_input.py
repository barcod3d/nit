def square_int(x):
    """
    Squares an integer x and prints the result.
    :param x: an integer x as user input.
    :type x: str
    """
    if x.isdigit():
        x = int(x)
        square_x = x * x
        print('{} squared is {}.'.format(x, square_x))
    else:
        print('{} is not a number.'.format(x))

def square(x):
    """
    Squares a number x and prints the result.
    :param x: a number x as user input.
    :type x: str
    """
    try:
        x = float(x)
        square_x = x * x
        print('{} squared is {}.'.format(x, square_x))
    except ValueError:
        print('{} is not a number.'.format(x))


# x = input("Enter a number: ")
# square(x)

def order(drink):
    """
    Orders the user a beer or prints 'unknown drink' if no beer is ordered.
    :param drink: The user's order
    :type drink: str
    """
    drink = trim(drink)
    if drink == 'beer':
        print('Successful order.')
    else:
        print('Unknown drink.')


def trim(string):
    """
    Trims a string of whitespaces and returns a lower case version of it.
    :param string: The string to trim.
    :type string: str
    :return: a trimmed lower case version of the input string.
    :rtype: str
    """
    string = string.lower()
    string = string.strip()
    return string


x = input("Enter drink: ")
order(x)