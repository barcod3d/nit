import ex_input
from functions.split import ex_split
from boolean.ex_logic import gruesse as greet_austrian

ex_input.order('beer')
greet_austrian(12)

# `Login` input area and processing
username = input("Enter your username: ")
password = input("Enter your password: ")

# login() will return a tuple (is_logged_in, output) and we want to print the output.
print(ex_split.login(username, password)[1])

