# IO - Input / output

## Input

So far we have learned basic calculations and output. However, applications typically do their tasks based on the user
input. System input via console is quite simple in Python:

```python
x = input("Enter a number: ")
```


### Defensive Programming

Processing user-input can be potentially very dangerous, because it might crash your application. The data type of the 
user input is typically a string, and it needs to be converted to whatever data type you want to work on. So the 
example will crash: 

```python
def square(x):
    square_x = x * x
    print('{} squared is {}.'.format(x, square_x))


square(x)
```

So the first thing we might do is to cast the value:

```python
def square(x):
    x = int(x)
    ...
```

However, the program can still crash, if the user enters anything but an integer, including characters or floating point
numbers. We can use a pre-check to validate the input: 

```python
def square(x):
    if x.isdigit():
        x = int(x)
        square_x = x * x
        print('{} squared is {}.'.format(x, square_x))
    else:
        print('{} is not a number.'.format(x))
```

However, if we still want to allow floats, we can use a `try` statement that catches certain errors: 

```python
def square(x):
    try:
        x = float(x)
        square_x = x * x
        print('{} squared is {}.'.format(x, square_x))
    except ValueError:
        print('{} is not a number.'.format(x))
```

This one will try to execute the code until a ValueError, e.g., if x cannot be converted to a float. If such an error 
happens the block in `except` will be executed. Note that `float()` will cause the first error, however `x * x` doesn't
work on strings either, so we do need that inside the try block, too. Furthermore, `square_x` is assigned in the try 
block, so if that error happens, it will never be assigned and therefore causes another error. 

### Comparing strings

String comparisons demand **all** characters of strings to be the exact same, including leading and trailing 
whitespaces, as well as capitalization and punctuation. 

```python
def order(drink):
    if drink == 'Beer':
        print('Successful order.')
    else:
        print('Unknown drink.')
```

In this example an input like `' Beer'` will fail, just like `'Beer '`. We can build in some safeguards tho:

```python
def order(drink):
    drink = trim(drink)
    if drink == 'beer':
        print('Successful order.')
    else:
        print('Unknown drink.')


def trim(string):
    string = string.lower()
    string = string.strip()
    return string
```


## Import

The `import <module>` statement is used to import other *modules* into your Python code per reference, either *
built-ins*, *your own* or modules from the internet, such as *repositories*. PyCharm installs these automatically, but 
you can also install it manually using the command line interface of your OS and pip `>pip install <module>`. 

Furthermore, all imports must be stated at the very top of your own module. 

### Modules and packages

Each of the `<filename>.py` file is considered a module which can be imported to your code using relative pathing with
the dot notation. Assume the following directory and file structure:

```
src/ 
|-- parent.py
|-- io/ 
    |-- current.py 
    |-- sibling.py
    |-- nested/ 
        |-- child.py
```

And you want to import into `current.py`:

- same directory level: `import sibling`
- parent directory level: `import .parent`
- child directory level: `import nested.child`

You can then use the module to call its function, e.g.:

```python
import ex_input


ex_input.order('beer')
```

#### Packages

When you take a closer look in PyCharm you see that the icon some folders in your project are having a dot. That means
they are packages that typically contain one or more modules. For simplicity Python packages are directories with
a `__init__.py` file, that can be used as import, too. You can use the following statement:
`from <package> import <module>`.

```python
from functions.split import ex_split

# `Login` input area and processing
username = input("Enter your username: ")
password = input("Enter your password: ")

# login() will return a tuple (is_logged_in, output) and we want to print the output.
print(ex_split.login(username, password)[1])
```

You can however also use this syntax to import only specific functions from a module `from <package>.<module> import
<function>`, while the package parameter is optional. Furthermore, you can rename imported functions and modules using
the `as` keyword:

```python
from boolean.ex_logic import gruesse as greet_austrian

greet_austrian(12)
```



