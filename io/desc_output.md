# IO - Input / output 

## Output

In the previous section we implicitly learned how to output something to a console using the statement 

```python
print('')
```

This might look underwhelming, but it's one of the most important concepts, especially early on. At some point in 
programming you might be wondering whether your code does what it's supposed to do. You could of course include software
tests or use debugging, but in order to just do a quick check most developers are quickly getting feedback by console
output. This is also especially useful to just check a small chunk of your actual routine, function or program.


