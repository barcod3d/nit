# simple greet function
def greet(name="World!"):
    print("Hello " + name)


def greet_last(*people):
    # tuples, lists and arrays are typically 0-indexed, so we need to subtract 1 of the total length to get the index.
    last_person_index = len(people) - 1
    print("Hello " + people[last_person_index])


def add(x, y):
    return x + y


greet("John")
greet()
greet_last("Max", "Joe", "Heidi")

x, y = 1, 2
print('The value of {0} + {1} is {2}.'.format(x, y, add(x, y)))


# show the scope
def show_local_x():
    x = 5
    return x


def show_global_x():
    global x
    x = 5
    return x


x = 3

print()
print('global x is {}'.format(x))
print('x will be assigned locally: {}'.format(show_local_x()))
print('but global x is still {}'.format(x))
print('x will be assigned using global keyword: {}'.format(show_global_x()))
print('global x is now {}'.format(x))