# Simple Functions  

Functions are modules of code that encapsulate a specific task which will only be executed on call. You can pass data, so called parameters or arguments and it can potentially return data as a result. 

## Definition and parameters
Functions are defined using the keyword `def` and they look like this:

```python
def greet(name = "World!"):
  print("Hello " + name)

greet("John")
greet()
``` 

This function has 1 optional parameter as input but it does not return data. Instead it just logs to the console. It is optional because you assign a default value. 

In contrast to other programming languages, python does not require you to have a specific amount of parameters or a specific datatype. Which is great but also has some risk for errors. If you want an arbitrary number of parameters you can do so with the asterix:

```python
def greet_last(*people):
  # tuples, lists and arrays are typically 0-indexed, therefore we need to substract 1 of the total length to get the index. 
  last_person_index = len(people) - 1
  print ("Hello " + people[last_person_index])

greet_last("Max", "Joe", "Heidi")
```

## Return values 
If we want to actually use the result of the function we have to return its data. To do so we can use the `return`
 statement: 
 
```python
def add(x, y): 
  return x + y

print(add(4,5))
```

## Scope of variables

Variables that are assigned at the root of your code will be accessible for all subsequent code. Variables assigned 
within functions are only accessible from within the function. However you can change that behavior using the `global`
keyword as demonstrated below. So be sure to know where your scopes and how to differ these variables :)

```python
# show the scope
def show_local_x():
    x = 5
    return x


def show_global_x():
    global x
    x = 5
    return x 


x = 3

print()
print('global x is {}'.format(x)) # 3
print('x will be assigned locally: {}'.format(show_local_x())) # 5
print('but global x is still {}'.format(x)) # 3
print('x will be assigned using global keyword: {}'.format(show_global_x())) # 5
print('global x is now {}'.format(x)) #5 
```
