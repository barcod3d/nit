# Ignore these for now - see lesson 5 :)
from binascii import hexlify
from hashlib import pbkdf2_hmac as encrypt

# Assume this is user input.
steve_name = 'HotSteve85'
steve_password = '1234'

# Assume this is from the database:
users = {
    1: {
        'username': 'HotSteve85',
        'password': "53f044806841f560e34c1aeb791d954c96f631d477ab661070644c3ac1c200e480a49d577c561a64fc30724ab93798"
                    "d1cf1b99dc585b414e910061eb3b7b9478f4606636299510e594dfdc9c5205a1ce1032263426762d067daa26387f7a9af5"
    }
}


def bad_login(username, password):
    """
    Bad code example: All in one login with no comments.
    """
    iterations = 100000
    # see loops in lesson 6 :)
    for user_id in users:
        if users[user_id]['username'] == username:
            salt = users[user_id]['password'][:64]
            db_pwd = users[user_id]['password'][64:]
            hashed_pwd = encrypt('sha512', password.encode('utf-8'), salt.encode('ascii'), iterations)
            hashed_pwd = hexlify(hashed_pwd).decode('ascii')
            if hashed_pwd == db_pwd:
                print('Successful login.')
            else:
                print('Wrong password.')
        else:
            print('Wrong username or password.')


def encrypt_password(salt, password):
    """
    Encrypts a given password with a given salt based on the SHA-512 encryption algorithm and returns it. You don't
    need to understand this method by now, this is just a close to real-life example.

    :param salt: The cryptographic salt, i.e., random data that is used as additional input to safeguard passwords.
    :type salt: str
    :param password: The cleartext password.
    :type password: str
    :return: The encrypted password.
    :rtype: str
    """
    iterations = 100000
    encrypted_password = encrypt('sha512', password.encode('utf-8'), salt.encode('ascii'), iterations)
    encrypted_password = hexlify(encrypted_password).decode('ascii')
    return encrypted_password


def verify_username(username):
    """
    Checks whether the user with the given username is registered.
    """
    for user_id in users:
        if users[user_id]['username'] == username:
            return users[user_id]
    return False


def verify_password(stored_password, password):
    """
    Checks whether a given password matches the stored password.

    :param stored_password: The password-hash string of the stored password.
    :type stored_password: str
    :param password: The plaintext password as user input.
    :type password: str
    :return: True, if the passwords match, False else.
    :rtype: bool
    """
    # the db password is 128 characters, i.e. bytes long, the first 64 being the password the latter being the salt.
    half = 64
    salt = stored_password[:half]
    stored_password = stored_password[half:]

    # Check whether the given password as encrypted password matches the stored password.
    return encrypt_password(salt, password) == stored_password


def login(username, password):
    """
    Logs a user with a given username and password in, i.e., checks whether the user has eligible access. This is done
    by outsourced functions.

    :param username: The user's username input.
    :type username: str
    :param password: The user's password input.
    :type password: str
    :return: True if the user logged in successfully, False else.
    :rtype: tuple
    """
    is_logged_in = False
    user = verify_username(username)

    # This is True if 'user' is not empty or False
    if user:
        if verify_password(user.get('password'), password):
            output = 'Successful login.'
            is_logged_in = True
        else:
            output = 'Wrong password.'
    else:
        output = 'Wrong username or password.'
    return is_logged_in, output


# For testing purposes you can adapt the username and password on lines 6 & 7 or see input. 
# bad_login(steve_name, steve_password)
# print(login(steve_name, steve_password)[1])
