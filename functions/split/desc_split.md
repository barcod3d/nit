# Split and merge

So, we haven't really talked about splitting and merging functions, yet but I think it will come handy now.

## Splitting functions

You can split functions into smaller ones. We already said that functions are modules of code that encapsulate a
specific task. If a function does more than one specific task or if the functions are relatively long, you should
consider splitting it. Basically there are several reasons to do so.

- *Readability*: Short pieces of code encapsulated in a function definition with a name that describes the functionality
is typically easier to read.
- *Redundancy*: We want to avoid duplicate code as much as possible, because this is giving us redundancy. Whenever we
want or have to revisit redundant code, chances are we have to do more work and also we might forget some part.
- *Testing*: We haven't talked about software tests yet, but smaller functions can be tested more easily.

### Example

Let's assume we are still working on the app, and we want users to login, given a username and password and both are
stored in the database. Of course, we don't save passwords directly for security reasons. Dependent on the user input,
i.e., username and password, we have typically multiple outputs.

```python
# Assume this is user input.
steve_name = 'HotSteve85'
steve_password = '1234'

# Assume this is from the database:
users = {
    1: {
        'username': 'HotSteve85',
        'password': "53f044806841f560e34c1aeb791d954c96f631d477ab661070644c3ac1c200e480a49d577c561a64fc30724ab93798"
                    "d1cf1b99dc585b414e910061eb3b7b9478f4606636299510e594dfdc9c5205a1ce1032263426762d067daa26387f7a9af5"
    }
}


def login(username, password):
    """
    Bad code example: All in one login with no comments.
    """
    iterations = 100000
    for user_id in users:
        if users[user_id]['username'] == username:
            salt = users[user_id]['password'][:64]
            db_pwd = users[user_id]['password'][64:]
            hashed_pwd = encrypt('sha512', password.encode('utf-8'), salt.encode('ascii'), iterations)
            hashed_pwd = hexlify(hashed_pwd).decode('ascii')
            if hashed_pwd == db_pwd:
                print('Successful login.')
            else:
                print('Wrong password.')
        else:
            print('Wrong username or password.')
```

Chances are you have trouble to understand this code as there are multiple statements you don't know yet and the code is
rather confusing. We can split the functionality into logically different parts:

```python
def encrypt_password(salt, password):
    """
    Encrypts a given password with a given salt based on the SHA-512 encryption algorithm and returns it. You don't
    need to understand this method by now, this is just a close to real-life example.
    """
    iterations = 100000
    encrypted_password = encrypt('sha512', password.encode('utf-8'), salt.encode('ascii'), iterations)
    encrypted_password = hexlify(encrypted_password).decode('ascii')
    return encrypted_password


def verify_username(username):
    """
    Checks whether the user with the given username is registered.
    """
    for user_id in users:
        if users[user_id]['username'] == username:
            return users[user_id]
    return False
```

## Merging functions

Good news is, we have been using this from the very get go, you just didn't realize. Some functions have an output
value, i.e., a value that is being returned from the function using the `return` statement. This value can be used
elsewhere in the code, for example, in other functions. So let's further split and merge the login function:

```python
def verify_password(stored_password, password):
    """
    Checks whether a given password matches the stored password.
    """
    # the db password is 128 characters, i.e. bytes long, the first 64 being the password the latter being the salt.
    half = 64
    salt = stored_password[:half]
    stored_password = stored_password[half:]

    # Check whether the given password as encrypted password matches the stored password.
    # Use the return value of encrypt_password() directly. 
    return encrypt_password(salt, password) == stored_password


def login(username, password):
    """
    Logs a user with a given username and password in, i.e., checks whether the user has eligible access. This is done
    by outsourced functions. 
    """
    is_logged_in = False
   # Store the return value of verfiy_username() in a variable. 
    user = verify_username(username)
    if user:
        # Use the return value of verify_password() directly. 
        if verify_password(user.get('password'), password):
            output = 'Successful login.'
            is_logged_in = True
        else:
            output = 'Wrong password.'
    else:
        output = 'Wrong username or password.'
    return is_logged_in, output
```